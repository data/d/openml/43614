# OpenML dataset: Crude-Oil-Mar-21-(CLF)-1-Feb-2021

https://www.openml.org/d/43614

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Crude oil price from Yahoo Finance as of 1 February 2021, 10:19PM EST. Market open.
NY Mercantile - NY Mercantile Delayed Price. Currency in USD. 
Content
Open: price at market opening
High: maximum price when the market open
Low: minimum price when the market open
Close: price at market closing
Adj Close: price after calculating company action at closing (i.e if there are stock dividend, the adj closing price will be changed)
Acknowledgements
Credits belong to Yahoo Finance, data taken from 
https://finance.yahoo.com/quote/CL3DF/history?p=CL3DF
Inspiration
Data for price prediction and analysis

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43614) of an [OpenML dataset](https://www.openml.org/d/43614). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43614/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43614/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43614/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

